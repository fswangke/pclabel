function [Ipos, Ineg] = esvm_generate_ImageNet_dataset(Npos, Nneg)
    pos_dir = 'data\ImageNet\Keyboard\n03085013\';
    pos_bbox_dir = 'data\ImageNet\keyboard\Annotation\n03085013\';
    neg_dir = 'data\ImageNet\Computer\n03082979\';
    
    pos_im_names = dir(fullfile(pos_dir, '*.jpeg'));
    neg_im_names = dir(fullfile(neg_dir, '*.jpeg'));

    pos_im_num = length(pos_im_names);
    neg_im_num = length(neg_im_names);
    
    Npos = min(Npos, pos_im_num);
    Nneg = min(Nneg, neg_im_num);

    Ipos = cell(Npos, 1);
    Ineg = cell(Nneg, 1);
    
    for i = 1 : Npos
        image_name = fullfile(pos_dir, pos_im_names(i).name);
        Ipos{i}.I = im2double(imread(image_name));
        recs.folder = '';
        recs.filename = '';
        recs.source = '';
        [recs.size.width,recs.size.height,recs.size.depth] = size(Ipos{i});
        recs.segmented = 0;
        recs.imgname = sprintf('%08d',i);
        recs.imgsize = size(Ipos{i});
        recs.database = '';
        
        object.class = 'keyboard';
        object.view = '';
        object.truncated = 0;
        object.occluded = 0;
        object.difficult = 0;
        object.label = 'keyboard';
        [~, fname, ~] = fileparts(image_name);
        bbox_fname = fullfile(pos_bbox_dir, [fname, '.xml']);
        if exist(bbox_fname, 'file')
            bb = xml_parseany(fileread(bbox_fname));
            bb = bb(1);
            object.bbox = [...
                str2double(bb.object{1}.bndbox{1}.ymin{1}.CONTENT),...
                str2double(bb.object{1}.bndbox{1}.xmin{1}.CONTENT),...
                str2double(bb.object{1}.bndbox{1}.ymax{1}.CONTENT),...
                str2double(bb.object{1}.bndbox{1}.xmax{1}.CONTENT)
                ];
        else
            [height, width, ~] = size(Ipos{i}.I);
            object.bbox = [1, 1, width, height];
        end
        object.bndbox.xmin =object.bbox(1);
        object.bndbox.ymin =object.bbox(2);
        object.bndbox.xmax =object.bbox(3);
        object.bndbox.ymax =object.bbox(4);
        object.polygon = [];
        recs.objects = [object];
        Ipos{i}.recs = recs;
    end
    
    for i = 1 : Nneg
        Ineg{i} = im2double(imread(fullfile(neg_dir, neg_im_names(i).name)));
    end
end