function superpixels = segmentToSuperPixels(img)
    sigma = 0.5;
    k = 500;
    min_size = 20;
    segment = segmentmex(img, sigma, k, min_size);
    values = unique(segment);
    superpixels = zeros(size(segment));
    for i = 1:length(values)
        superpixels(segment==values(i)) = i;
    end
end