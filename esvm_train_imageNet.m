function [models, M] = esvm_train_imageNet
    addpath(genpath('./exemplar_svm/'));
    addpath(genpath('./xml_toolbox/'));
    Npos = 500;
    Nneg = 100;
    [pos_set, neg_set] = esvm_generate_ImageNet_dataset(Npos, Nneg);

    models_name = 'keyboard';

    params = esvm_get_default_params;
    params.init_params.sbin = 4;
    params.init_params.MAXDIM = 6;
    params.model_type = 'exemplar';

    %enable display so that nice visualizations pop up during learning
    params.dataset_params.display = 1;

    %if localdir is not set, we do not dump files
    %params.dataset_params.localdir = '/nfs/baikal/tmalisie/synthetic/';

    %%Initialize exemplar stream
    stream_params.stream_set_name = 'trainval';
    stream_params.stream_max_ex = 10;
    stream_params.must_have_seg = 0;
    stream_params.must_have_seg_string = '';
    stream_params.model_type = 'exemplar'; %must be scene or exemplar
    %assign pos_set as variable, because we need it for visualization
    stream_params.pos_set = pos_set;
    stream_params.cls = models_name;
    
    e_stream_set = esvm_get_pascal_stream(stream_params, params.dataset_params);
    
    val_neg_set = neg_set((Nneg/2+1):end);
    neg_set = neg_set(1:((Nneg/2)));

    initial_models = esvm_initialize_exemplars(e_stream_set, params, models_name);
    
    train_params = params;
    train_params.detect_max_scale = 1.0;
    train_params.train_max_mined_images = 50;
    train_params.detect_exemplar_nms_os_threshold = 1.0;
    train_params.detect_max_windows_per_exemplar = 100;
    
    [models] = esvm_train_exemplars(initial_models, neg_set, train_params);
    
    val_params = params;
    val_params.detect_exemplar_nms_os_threshold = 0.5;
    val_params.gt_function = @esvm_load_gt_function;
    val_set = cat(1, pos_set(1:Npos/2), val_neg_set(:));
    val_set_name = 'valset';
    
    val_grid = esvm_detect_imageset(val_set, models, val_params, val_set_name);

    M = esvm_perform_calibration(val_grid, val_set, models, val_params);
    
    test_set = pos_set;
    test_params = params;
    test_params.detect_exemplar_nms_os_threshold = 0.5;
    test_set_name = 'testset';

    test_grid = esvm_detect_imageset(test_set, models, test_params, test_set_name);
    
    test_struct = esvm_pool_exemplar_dets(test_grid, models, M, test_params);

    maxk = 20;
    allbbs = esvm_show_top_dets(test_struct, test_grid, test_set, models, params, maxk, test_set_name);

end